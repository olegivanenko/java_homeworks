package homework9;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class FamilyTest {

    private Human mother = new Human("Maria", "Morris", 1885);
    private Human father = new Human("Mark", "Morris", 1933);
    private Family family = new Family(mother, father);
    private Human anna = new Human("Anna", "Morris", 1995);
    private Human pavel = new Human("Pavel", "Morris", 1990);

    @Test
    public void should_ReturnNumberOfFamily_Members() {
        //given
        // when
        int result = family.countFamilyMembers(new Human[2]);

        //then
        Assert.assertEquals(4, result);
    }

    @Test
    public void should_Return_Definite_String() {
        //given
        Family family = new Family(mother, father);
        // when
        String expected = family.toString();

        //then
        Assert.assertEquals(expected, this.family.toString());

        //assertThat(family.toString())
        //.contains("mother", "father");
    }

    @Test
    public void should_Delete_Child_WhenCorrect_Object_IsEntered() {
        //given
        family.addChild(anna);
        // when
        family.removeChild(anna);
        //then
        Assert.assertEquals(0, family.getChildren().length);

        family.addChild(anna);
        family.removeChild(new Human("Peter", "Morris", 1009));
        Assert.assertEquals(1, family.getChildren().length);

    }

    @Test
    public void should_ReturnTrue_when_CorrectChild_IsEntered_AndDeleted() {
        //given
        family.addChild(pavel);

        // when
        family.removeChild(pavel);
        boolean result = Arrays.asList(family.getChildren()[0]).contains(null);

        //then
        Assert.assertTrue(result);
    }

    @Test
    public void should_ReturnFalse_when_IncorrectObjectOfChild_IsTried_ToBeDeleted() {
        //given
        family.addChild(pavel);

        // when
        family.removeChild(new Human("Liza", "Johnson", 2000));
        boolean result = Arrays.asList(family.getChildren()[0]).contains(null);

        //then
        Assert.assertFalse(result);
    }

    @Test
    public void should_Delete_Child_WhenCorrect_IndexOfObjext_IsPointed() {
        //given
        family.addChild(pavel);
        // when
        family.removeChild(0);
        //then
        assertThat(family.toString())
                .doesNotContain("Pavel");
    }

    @Test
    public void should_Delete_NoChild_WhenIncorrect_IndexOfObjext_IsPointed() {
        //given
        family.addChild(pavel);
        // when
        family.removeChild(5);
        //then
        assertThat(family.toString())
                .contains("Pavel");
    }

    @Test
    public void should_ReturnTrue_when_Child_WithCorrect_IndexOfObjext_IsDeleted() {
        //given
        family.addChild(pavel);

        // when
        family.removeChild(0);
        boolean result = Arrays.asList(family.getChildren()[0]).contains(null);

        //then
        Assert.assertTrue(result);
    }

    @Test
    public void should_ReturnFalse_when_Child_WithIncorrect_IndexOfObjext_IsTriedToBeDeleted() {
        //given
        family.addChild(pavel);

        // when
        family.removeChild(3);
        boolean result = Arrays.asList(family.getChildren()[0]).contains(null);

        //then
        Assert.assertFalse(result);
    }

    @Test
    public void should_AddChild_AndReturn_NewArray_WithCorrectObject() {
        //given
        // when
        family.addChild(pavel);
        family.addChild(anna);
        //then
        assertThat(family.getChildren()).hasSize(2)
                .contains(pavel, anna);
    }

    @Test
    public void should_ReturnFalse_WhenObject_EqualsNull() {
        //given
        Family nullFamily = null;
        // when
        //then
        assertThat(family.equals(nullFamily)).isFalse();
    }

    @Test
    public void should_ReturnTrue_WhenTwoEqualObjects_HaveEqualHashCodes() {
        //given
        Family family1 = new Family(mother, father);
        Family family2 = new Family(mother, father);
        // when
        //then
        Assert.assertTrue(family1.hashCode()==(family2.hashCode()));
    }


//    @Override
//    public String toString() {
//        return "FamilyTest{" +
//                "mother=" + mother +
//                ", father=" + father +
//                ", anna=" + anna +
//                ", pavel=" + pavel +
//                '}';
//    }
//
//    public static void main(String[] args) {
//
//
//        Family family1 = new Family(mother, father);
//
//        family1.toString();
}

//given
// when
//then
