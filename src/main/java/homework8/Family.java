package homework8;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.Random;

public class Family {

    private Human mother;
    private Human father;
    private Human[] children = new Human[0];
    private Pet pet;

    static {
        System.out.println("Class Family is initialized");
    }

    {
        System.out.println("Object Family is initialized");
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
    }

    public void addChild(Human child) {
        Human[] newChildren = new Human[this.children.length + 1];
        for (int i = 0; i < this.children.length; i++) {
            if (children[i] != null)
                newChildren[i] = children[i];
        }
        newChildren[this.children.length] = child;
        this.children = newChildren;
    }


    public boolean removeChild(int index) {
        if (children.length - 1 < index || index < 0) {
            return false;
        }
        Human[] result = new Human[children.length - 1];
        int indexOfChildForRemoving = 0;
        for (int i = 0; i < children.length; i++) {
            if (i != index) {
                result[indexOfChildForRemoving++] = children[i];
            }
        }
        this.children = result;
        return true;
    }

    public boolean removeChild(Human child) {
        if (children == null || children.length == 0) {
            return false;
        }
        boolean isChildRemoved = false;
        long childHashcode = child.hashCode();
        for (int i = 0; i < children.length; i++) {
            if (childHashcode == children[i].hashCode()) {
                if (child.equals(children[i])) {
                    isChildRemoved = removeChild(i);
                    break;
                }
            }
        }
        return isChildRemoved;
    }

    public int countFamilyMembers(Human[] children) {
        return children.length + 2;
    }

    public void greetPet() {
        System.out.println("Hi, " + this.getPet().getNickname() + " !");
    }

    public void describerPet() {
        System.out.println("We have a " + this.getPet().getSpecies() + ". " + "It is " + this.getPet().getAge
                () +
                " years old. " + "It is " + this.getPet().getTrickLevel() + ".");
    }

    public boolean feedPet(boolean timeToFeedPet) {
        boolean isPetFeeden = false;
        if (timeToFeedPet) {
            System.out.println("hmmm... We should feed " + this.getPet().getNickname());
            isPetFeeden = true;
        } else {
            int number = generateRandomIntInRange();
            if (number < this.getPet().getTrick()) {
                System.out.println("hmmm... We should feed " + this.getPet().getNickname());
                isPetFeeden = true;
            } else {
                System.out.println("We think " + this.getPet().getNickname() + " is not too hungry");
            }
        }
        return isPetFeeden;
    }

    public static int generateRandomIntInRange() {
        Random r = new Random();
        return r.nextInt(101);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("mother: ").append(mother).
                append("\n\nfather: ").append(father).
                append("\n\nchildren: ").append(Arrays.deepToString(children)).
                append("\n\nnpet: ").append(pet);
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother);
        result = 31 * result + Objects.hash(father);
        return result;
    }

    protected void finalize ( ) {
        System.out.println(this);
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Human[] getChildren() {
        return children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

}
