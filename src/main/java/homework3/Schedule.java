package homework3;

import java.util.Scanner;

public class Schedule {
    static String[][] schedule = new String[7][2];

    public static void main(String[] args) {

        schedule[0][0] = "Sunday";
        schedule[0][1] = "to spend time with family";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to IT courses";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "do home works";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "visit DAN-IT";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "finish project at my work";
        schedule[5][0] = "Friday";
        schedule[5][1] = "self-education, to learn materials";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "all day in DAN-IT at courses))";
        //System.out.println(schedule[6][1]);
        getInfoFromSchedule(schedule);

    }

    public static void getInfoFromSchedule(String schedule[][]) {
        System.out.println("Please, input the day of the week :");
        Scanner in = new Scanner(System.in);
        String weekDay = in.next().toLowerCase();

        Scanner inside = new Scanner(System.in);

        while (true) {
            if (weekDay.equals("sunday") || weekDay.equals("monday") || weekDay.equals("tuesday") || weekDay.equals
                    ("wednesday")
                    || weekDay.equals("thursday") || weekDay.equals("friday") || weekDay.equals("saturday")) {
                switch (weekDay) {
                    case "sunday":
                        System.out.println("Your task for Sunday: " + schedule[0][1]);
                        System.out.println("Please, input next day of the week or enter exit to terminate the " +
                                "programme");
                        weekDay = inside.next().toLowerCase();
                        break;
                    case "monday":
                        System.out.println("Your task for Monday: " + schedule[1][1]);
                        System.out.println("Please, input next day of the week or enter exit to terminate the " +
                                "programme");
                        weekDay = inside.next().toLowerCase();
                        break;
                    case "tuesday":
                        System.out.println("Your task for Tuesday: " + schedule[2][1]);
                        System.out.println("Please, input next day of the week or enter exit to terminate the " +
                                "programme");
                        weekDay = inside.next().toLowerCase();
                        break;
                    case "wednesday":
                        System.out.println("Your task for Wednesday: " + schedule[3][1]);
                        System.out.println("Please, input next day of the week or enter exit to terminate the " +
                                "programme");
                        weekDay = inside.next().toLowerCase();
                        break;
                    case "thursday":
                        System.out.println("Your task for Thursday: " + schedule[4][1]);
                        System.out.println("Please, input next day of the week or enter exit to terminate the " +
                                "programme");
                        weekDay = inside.next().toLowerCase();
                        break;
                    case "friday":
                        System.out.println("Your task for Friday: " + schedule[5][1]);
                        System.out.println("Please, input next day of the week or enter exit to terminate the " +
                                "programme");
                        weekDay = inside.next().toLowerCase();
                        break;
                    case "saturday":
                        System.out.println("Your task for Saturday: " + schedule[6][1]);
                        System.out.println("Please, input next day of the week or enter exit to terminate the " +
                                "programme");
                        weekDay = inside.next().toLowerCase();
                        break;
                }
            } else if (weekDay.equals("exit")) {
                return;
            } else {
                System.out.println("Sorry, I don't understand you, please try again.");
                weekDay = inside.next().toLowerCase();
            }
        }
    }
}
