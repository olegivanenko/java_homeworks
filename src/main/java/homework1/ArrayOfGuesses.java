package homework1;

import java.util.Random;
import java.util.Scanner;

public class ArrayOfGuesses {

    public static void main(String[] args) {
        int num = generateRandomIntInRange();
        //System.out.println(num);
        enterYourName();
        System.out.println("Let the game begin!");
        guessNumber(num);
    }

    private static int generateRandomIntInRange() {
        Random r = new Random();
        return r.nextInt((100 - 0) + 1) + 0;
    }

    private static String enterYourName() {
        System.out.print("Enter your name: ");
        Scanner in = new Scanner(System.in);
        String userName = in.next();
        return userName;
    }

    private static void guessNumber(int generatedNumber) {
        int numberFiller = -1;
        int[] allUserNumbers = new int[25];

        for (int i = 0; i < allUserNumbers.length; i++) {
            allUserNumbers[i] = numberFiller;
        }
        int countOfIterations = 0;
        System.out.println("Enter a number from 0 to 100 (including)");
        Scanner in = new Scanner(System.in);
        int userNumber = in.nextInt();

        while (userNumber != generatedNumber) {
            if (userNumber < generatedNumber) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (userNumber > generatedNumber) {
                System.out.println("Your number is too big. Please, try again.");
            }
            allUserNumbers[countOfIterations] = userNumber;
            countOfIterations++;
            Scanner inside = new Scanner(System.in);
            userNumber = inside.nextInt();
        }
        if (userNumber == generatedNumber) {
            allUserNumbers[countOfIterations] = userNumber;
            System.out.println("Congratulations! You number " + userNumber + " is correct!");
            System.out.println("Your entered numbers are: " + sortArray(allUserNumbers, numberFiller));
        }
    }

    private static String sortArray(int[] arrayToSort, int maxNumber) {
        boolean isSorted = false;
        int buf;
        String outputString = "";

        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < arrayToSort.length - 1; i++) {
                if (arrayToSort[i] > arrayToSort[i + 1]) {
                    isSorted = false;

                    buf = arrayToSort[i];
                    arrayToSort[i] = arrayToSort[i + 1];
                    arrayToSort[i + 1] = buf;
                }
            }
        }

        for (int i = 0; i < arrayToSort.length; i++) {
            if (arrayToSort[i] != maxNumber) {
                outputString += " " + arrayToSort[i];
            }
        }
        return outputString;
    }

}
