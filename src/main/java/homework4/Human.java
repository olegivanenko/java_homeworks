package homework4;

import java.util.Arrays;
import java.util.Random;

public class Human {

    protected final String name;
    protected final String surname;
    protected final int yearOfBirth;
    protected int iq;
    protected Pet currentPet;
    protected final Human mother;
    protected final Human father;
    protected String[][] schedule;
    protected boolean timeToFeedPet = true;

    public Human(String name, String surname, int yearOfBirth, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, int yearOfBirth) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
        this.mother = null;
        this.father = null;
    }

    public void greetPet() {
        System.out.println("Hi, " + currentPet.getNickname() + " !");
    }

    public void describerPet() {
        System.out.println("I have a " + currentPet.getSpecies() + ". " + "It is " + currentPet.getAge() +
                " years old. " + "It is " + currentPet.getTrickLevel() + ".");
    }

    public boolean feedPet(boolean timeToFeedPet) {
        boolean isPetFeeden = false;
        if ( timeToFeedPet ) {
            System.out.println("hmmm... I should feed " + currentPet.getNickname());
            isPetFeeden = true;
        } else {
            int number = generateRandomIntInRange();
            if (number < currentPet.getTrick()) {
                System.out.println("hmmm... I should feed " + currentPet.getNickname());
                isPetFeeden = true;
            } else {
                System.out.println("I think " + currentPet.getNickname() + " is not too hungry");
            }
        }
        return isPetFeeden;
    }

    public static int generateRandomIntInRange() {
        Random r = new Random();
        return r.nextInt(101);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("name: ").append(name).
                append("\nsurname: ").append(surname).
                append("\nyear of birth: ").append(yearOfBirth).
                append("\niq: ").append(iq).
                append("\npet: ").append(currentPet).
                append("\nmother: ").append(mother).
                append("\nfather: ").append(father).
                append("\nschedule: ").append(Arrays.toString(schedule));
        return sb.toString();
    }

    public boolean getTimeToFeedPet() {
        return timeToFeedPet;
    }

    public void setTimeToFeedPet(boolean timeToFeed) {
        timeToFeedPet = timeToFeed;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setCurrentPet(Pet currentPet) {
        this.currentPet = currentPet;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }
}
//        описать своего любимца (describePet): (вывести на экран всю информацию о своем питомце: "У меня есть [вид животного], ему [возраст животного] лет, он [очень хитрый]/[почти не хитрый]". Степень описания хитрости должна зависеть от уровня хитрости питомца, если больше 50 - [очень хитрый], если меньше или равно 50 - [почти не хитрый])//

//    Имя (name)
//    Фамилия (surname)
//    Год рождения (year), число
//    Уровень IQ (iq) (целое число от 0 до 100)
//        Домашний любимец (currentPet) (объект типа Pet)
//        Мама (mother) (объект типа Human)
//        Папа (father) (объект типа Human)
//        Расписание внерабочих занятий (schedule) (2-мерный массив: [день недели] x [тип секции/отдыха])


//        поприветствовать своего любимца (greetPet)("Привет, [имя животного]")
