package homework4;

public class Main {

    public static void main(String[] args) {

        String[] newHabits = new String[]{ "eat", "drink", "sleep", "play"};

        String[][] scheduleAdult = new String[7][2];
        scheduleAdult[0][0] = "Sunday";
        scheduleAdult[0][1] = "to spend time with family";
        scheduleAdult[1][0] = "Monday";
        scheduleAdult[1][1] = "swimming pool";
        scheduleAdult[2][0] = "Tuesday";
        scheduleAdult[2][1] = "cinema";
        scheduleAdult[3][0] = "Wednesday";
        scheduleAdult[3][1] = "reading";
        scheduleAdult[4][0] = "Thursday";
        scheduleAdult[4][1] = "sauna";
        scheduleAdult[5][0] = "Friday";
        scheduleAdult[5][1] = "restaurant";
        scheduleAdult[6][0] = "Saturday";
        scheduleAdult[6][1] = "art gallery";

        String[][] scheduleChild = new String[7][7];
        scheduleChild[0][0] = "Sunday";
        scheduleChild[0][1] = "play with parents";
        scheduleChild[1][0] = "Monday";
        scheduleChild[1][1] = "swimming pool";
        scheduleChild[2][0] = "Tuesday";
        scheduleChild[2][1] = "cinema";
        scheduleChild[3][0] = "Wednesday";
        scheduleChild[3][1] = "reading";
        scheduleChild[4][0] = "Thursday";
        scheduleChild[4][1] = "visit music school";
        scheduleChild[5][0] = "Friday";
        scheduleChild[5][1] = "visit grandparents";
        scheduleChild[6][0] = "Saturday";
        scheduleChild[6][1] = "visit art gallery";

        Pet dogLessi = new Pet("dog", "Lessi", 2, 50, "eat", "drink");
        Human mammyAlla = new Human("Alla", "Smirnova", 1980);
        Human dadPetr = new Human("Petr", "Smirnov", 1975);
        Human daughterLiza = new Human("Liza", "Smirnova", 2005, mammyAlla, dadPetr);

        mammyAlla.setIq(90);
        dadPetr.setIq(95);
        daughterLiza.setIq(90);

        mammyAlla.setSchedule(scheduleAdult);
        dadPetr.setSchedule(scheduleAdult);
        daughterLiza.setSchedule(scheduleChild);

        daughterLiza.setCurrentPet(dogLessi);

        daughterLiza.greetPet();
        daughterLiza.describerPet();
        System.out.println();

        dogLessi.eat();
        dogLessi.respond();
        dogLessi.foul();
        System.out.println();

        daughterLiza.setTimeToFeedPet(false);
        daughterLiza.feedPet(daughterLiza.getTimeToFeedPet());

        System.out.println(daughterLiza);

    }

}
