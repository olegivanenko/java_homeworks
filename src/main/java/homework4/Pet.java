package homework4;

import java.lang.*;
import java.util.Arrays;

public class Pet {

    protected final String species;
    protected final String nickname;
    protected int age;
    protected int trickLevel;
    protected String[] habits;

    Pet(String species, String nickname, int age, int trickLevel, String... habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void eat() {
        System.out.println("I am eating...");
    }

    public void respond() {
        System.out.println(String.format("Hi, Master! I am - %s. I missed you.", nickname));
    }

    public void foul() {
        System.out.println("I should hide it ...");
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("species: ").append(species).
                append("\nnickname: ").append(nickname).
                append("\nage: ").append(age).
                append("\ntrickLevel: ").append(trickLevel).
                append("\nhabits: ").append(Arrays.toString(habits));
        return sb.toString();
    }

    public String getTrickLevel() {
        if (trickLevel <= 50) {
            return "not very sly";
        } else {
            return "very-very sly";
        }
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String getNickname() {
        return nickname;
    }

    public String getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public int getTrick() {
        return trickLevel;
    }

}
//    private final String species;
//    private final String nickname;
//    private int age;
//    private int trickLevel;
//    String[] habits;

//        вид животного (species), строка (собака, кот и тд)
//        кличка (nickname)
//        возраст (age)
//        уровень хитрости (trickLevel) (целое число от 0 до 100)
//        привычки (habits) (массив строк)


//        покушать (eat) (метод выводит на экран сообщение Я кушаю!)
//        отозваться (respond) (метод выводит на экран сообщение Привет, хозяин. Я - [имя животного]. Я соскучился!)
//        сделать домашнюю гадость (foul) (метод выводит на экран сообщение Нужно хорошо замести следы...)