package homework6;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {

    private String name;
    private String surname;
    private Family family;
    private int yearOfBirth;
    private int iq;
    private String[][] schedule;

    public Human(String name, String surname, int yearOfBirth) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
    }

    public Human(String name, String surname, int yearOfBirth, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human() {
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("name: ").append(name).
                append("\nsurname: ").append(surname).
                append("\nyear of birth: ").append(yearOfBirth).
                append("\niq: ").append(iq).
                append("\nschedule: ").append(Arrays.deepToString(schedule));
        return sb.toString();
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public Family getFamily() {
        return family;
    }

    public int getIq() {
        return iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setFamily(Family family) { this.family = family; }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }
}

