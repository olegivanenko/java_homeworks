package homework6;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.Random;

public class Family {

    private Human mother;
    private Human father;
    private Human[] children = new Human[0];
    private Pet pet;


    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
    }

    public void addChild(Human child) {
        Human[] newChildren = new Human[this.children.length + 1];

        for (int i = 0; i < this.children.length; i++) {
            if (children[i] != null)
                newChildren[i] = children[i];
        }
        newChildren[this.children.length] = child;
        this.children = newChildren;
    }


    public boolean removeChild(int index) {
        if (children.length - 1 < index || index < 0) {
            return false;
        }
        Human[] result = new Human[children.length - 1];
        int indexOfChildForRemoving = 0;
        for (int i = 0; i < children.length; i++) {
            if (i != index) {
                result[indexOfChildForRemoving++] = children[i];
                //System.arraycopy(children, i, result, j++, 1);
            }
        }
        this.children = result;
        return true;
    }

    public boolean removeChild(Human child) {
        if (children == null || children.length == 0) {
            return false;
        }
        boolean isChildRemoved = false;
        for (int i = 0; i < children.length; i++) {
            if (children[i].getName().equals(child.getName()) &&
                    children[i].getSurname().equals(child.getSurname()) &&
                    children[i].getYearOfBirth() == child.getYearOfBirth()) {
                isChildRemoved = removeChild(i);
                break;
            }
        }
        return isChildRemoved;
    }

    public int countFamilyMembers(Human[] children) {
        return children.length + 2;
    }

    public void greetPet() {
        System.out.println("Hi, " + this.getPet().getNickname() + " !");
    }

    public void describerPet() {
        System.out.println("We have a " + this.getPet().getSpecies() + ". " + "It is " + this.getPet().getAge
                () +
                " years old. " + "It is " + this.getPet().getTrickLevel() + ".");
    }

    public boolean feedPet(boolean timeToFeedPet) {
        boolean isPetFeeden = false;
        if (timeToFeedPet) {
            System.out.println("hmmm... We should feed " + this.getPet().getNickname());
            isPetFeeden = true;
        } else {
            int number = generateRandomIntInRange();
            if (number < this.getPet().getTrick()) {
                System.out.println("hmmm... We should feed " + this.getPet().getNickname());
                isPetFeeden = true;
            } else {
                System.out.println("We think " + this.getPet().getNickname() + " is not too hungry");
            }
        }
        return isPetFeeden;
    }

    public static int generateRandomIntInRange() {
        Random r = new Random();
        return r.nextInt(101);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("mother: ").append(mother).
                append("\n\nfather: ").append(father).
                append("\n\nchildren: ").append(Arrays.deepToString(children)).
                append("\n\nnpet: ").append(pet);
        return sb.toString();
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Human[] getChildren() {
        return children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

}
