package homework9;

public class Main {

    static {
        System.out.println("Class Main is initialized");
    }

    public static void main(String[] args) {

        String[] newHabits = new String[]{"eat", "drink", "sleep", "play"};

        String[][] scheduleAdult = new String[7][2];
        scheduleAdult[0][0] = DayOfWeek.SUNDAY.name();
        scheduleAdult[0][1] = "to spend time with family";
        scheduleAdult[1][0] = DayOfWeek.MONDAY.name();
        scheduleAdult[1][1] = "swimming pool";
        scheduleAdult[2][0] = DayOfWeek.TUESDAY.name();
        scheduleAdult[2][1] = "cinema";
        scheduleAdult[3][0] = DayOfWeek.WEDNESDAY.name();
        scheduleAdult[3][1] = "reading";
        scheduleAdult[4][0] = DayOfWeek.THURSDAY.name();
        scheduleAdult[4][1] = "sauna";
        scheduleAdult[5][0] = DayOfWeek.FRIDAY.name();
        scheduleAdult[5][1] = "restaurant";
        scheduleAdult[6][0] = DayOfWeek.SATURDAY.name();
        scheduleAdult[6][1] = "art gallery";

        String[][] scheduleChild = new String[7][2];
        scheduleChild[0][0] = DayOfWeek.SUNDAY.name();
        scheduleChild[0][1] = "play with parents";
        scheduleChild[1][0] = DayOfWeek.MONDAY.name();
        scheduleChild[1][1] = "swimming pool";
        scheduleChild[2][0] = DayOfWeek.TUESDAY.name();
        scheduleChild[2][1] = "cinema";
        scheduleChild[3][0] = DayOfWeek.WEDNESDAY.name();
        scheduleChild[3][1] = "reading";
        scheduleChild[4][0] = DayOfWeek.THURSDAY.name();
        scheduleChild[4][1] = "visit music school";
        scheduleChild[5][0] = DayOfWeek.FRIDAY.name();
        scheduleChild[5][1] = "visit grandparents";
        scheduleChild[6][0] = DayOfWeek.SATURDAY.name();
        scheduleChild[6][1] = "visit art gallery";

        Pet dogLessi = new Pet(Species.DOG, "Lessi", 2, 50, "eat", "drink");
        Pet catRoger = new Pet(Species.CAT, "Roger", 1, 99, "eat", "sleep");
        Human mammyAlla = new Human("Alla", "Smirnova", 1980);
        Human dadPetr = new Human("Petr", "Smirnov", 1975);
        Human daughterLiza = new Human("Liza", "Smirnova", 2005);

        mammyAlla.setIq(90);
        dadPetr.setIq(95);
        daughterLiza.setIq(90);

        mammyAlla.setSchedule(scheduleAdult);
        dadPetr.setSchedule(scheduleAdult);
        daughterLiza.setSchedule(scheduleChild);

        System.out.println();

        dogLessi.eat();
        dogLessi.respond();
        dogLessi.foul();
        System.out.println();

        System.out.println(daughterLiza);

        Pet catTom = new Pet(Species.CAT, "Tom");
        Pet guinea_pigJerry = new Pet();
        Human mammyVera = new Human("Vera", "Ivanova", 1972, 85, scheduleAdult);
        Human dadVictor = new Human("Victor", "Ivanov", 1970, 99, scheduleAdult);
        Human sonAlex = new Human("Alex", "Ivanov", 1990, 91, scheduleChild);
        Human daughterMaria = new Human("Maria", "Ivanova", 1995, 98, scheduleChild);
        Human sonKirill = new Human("Kirill", "Ivanov", 1999);
        Human daughterAnna = new Human();

        System.out.println();

        System.out.println(catTom);
        System.out.println(guinea_pigJerry);
        System.out.println(mammyVera);
        System.out.println(dadVictor);
        System.out.println(sonAlex);
        System.out.println(daughterMaria);
        System.out.println(sonKirill);
        System.out.println(daughterAnna);

        System.out.println();

        Family smirnovy = new Family(mammyAlla, dadPetr);
        Family ivanovy = new Family(mammyVera, dadVictor);
        smirnovy.addChild(daughterLiza);
        ivanovy.addChild(sonAlex);
        ivanovy.addChild(daughterMaria);
        ivanovy.addChild(sonKirill);
        ivanovy.addChild(daughterAnna);

        smirnovy.setPet(catRoger);
        smirnovy.greetPet();
        smirnovy.describerPet();

        System.out.println(smirnovy.feedPet(false));

        System.out.println();

        ivanovy.removeChild(1);
        ivanovy.removeChild(2);
        ivanovy.removeChild(20);

        System.out.println(ivanovy.removeChild(sonAlex));
        System.out.println(ivanovy.removeChild(sonKirill));
        System.out.println(ivanovy.removeChild(sonAlex));
        System.out.println(ivanovy.removeChild(0));

        System.out.println(ivanovy);

        System.out.println();

        System.out.println(ivanovy.countFamilyMembers(ivanovy.getChildren()));

//        int i=0;
//        while(i < 10_000_000) {
//
//            new Human();
//
//                System.out.println("Total: "+Runtime.getRuntime().totalMemory()+
//                        "; free: "+Runtime.getRuntime().freeMemory());
//        }



    }
}
