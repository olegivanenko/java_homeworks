package homework2;

import java.util.Random;
import java.util.Scanner;

public class Target {

    public static void main(String[] args) {

        int targetMaxX = 5;
        int targetMaxY = 5;

        int targetMarkLine = createTargetMarkX(targetMaxX);
        int targetMarkColumn = createTargetMarkY(targetMaxY);

        char[][] targetWithMark = createTarget();
        //System.out.println(targetMarkLine);
        //System.out.println(targetMarkColumn);

        for (char[] s : targetWithMark) {
            System.out.println(s);
        }
        System.out.println("All set. Get ready to rumble!");
        getShooting(targetMarkLine, targetMarkColumn, targetWithMark);
    }


    private static char[][] createTarget() {
        char[][] targetArray = new char[5][5];

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                targetArray[i][j] = '-';
            }
        }

        return targetArray;
    }

    private static int createTargetMarkX(int maxX) {
        Random x = new Random();
        return x.nextInt(maxX);
    }

    private static int createTargetMarkY(int maxY) {
        Random y = new Random();
        return y.nextInt(maxY);
    }

    private static void getShooting(int targetMarkLine, int targetMarkColumn, char[][] targetWithMark) {
        System.out.println("Make your shoot: enter x and y coordinates from 0 to 4");
        Scanner in = new Scanner(System.in);
        int userShootX = in.nextInt();
        int userShootY = in.nextInt();

        while (true) {

            if (userShootX == targetMarkLine && userShootY == targetMarkColumn) {
                targetWithMark[userShootX][userShootY] = 'X';
                for (char[] s : targetWithMark) {
                    System.out.println(s);
                }
                System.out.println("you won!");
                break;
            } else {
                System.out.println("shoot one more time!");
                targetWithMark[userShootX][userShootY] = '*';
                for (char[] s : targetWithMark) {
                    System.out.println(s);
                }

                Scanner inside = new Scanner(System.in);
                userShootX = inside.nextInt();
                userShootY = inside.nextInt();
            }
        }

    }


}
