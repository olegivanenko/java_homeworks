package homework7;

import java.lang.*;
import java.util.Arrays;
import java.util.Objects;

public class Pet {

    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static { System.out.println("Class Pet is initialized"); }

    { System.out.println("Object Pet is initialized"); }

    public Pet(String species, String nickname, int age, int trickLevel, String... habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public void eat() {
        System.out.println("I am eating...");
    }

    public void respond() {
        System.out.println(String.format("Hi, Master! I am - %s. I missed you.", nickname));
    }

    public void foul() {
        System.out.println("I should hide it ...");
    }

    public String getTrickLevel() {
        if (trickLevel <= 50) {
            return "not very sly";
        } else {
            return "very-very sly";
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("species: ").append(species).
                append("\nnickname: ").append(nickname).
                append("\nage: ").append(age).
                append("\ntrickLevel: ").append(trickLevel).
                append("\nhabits: ").append(Arrays.toString(habits));
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(species);
        result = 31 * result + Arrays.hashCode(new String[]{nickname});
        return result;
    }

    public String getNickname() {
        return nickname;
    }

    public String getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public int getTrick() {
        return trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }
}
